import tensorflow as tf
import tensorboard
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision as tv

tf.io.gfile = tensorboard.compat.tensorflow_stub.io.gfile


class LeNet_BN(nn.Module):
    """ Simple model to label results.
    LeNet (32x32 input) with Batch Normalization.
    """
    def __init__(self, input_channels=1, output_classes=10):
        super().__init__()
        self.convolutions = nn.Sequential(
            nn.Conv2d(input_channels, 6, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(6),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(6, 16, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(16, 120, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(120),
            nn.ReLU(),
        )
        self.linear = nn.Sequential(
            nn.Linear(120, 84, bias=False),
            nn.BatchNorm1d(84),
            nn.ReLU(),
            nn.Linear(84, output_classes),
        )

    def forward(self, x: torch.Tensor):
        x = F.interpolate(x, size=(32, 32), mode='nearest')  # nearest | linear | bilinear | bicubic | trilinear

        x = self.convolutions(x)
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x



def add_embedding(
    generator, 
    summary_writer, 
    h_dim, 
    tag, 
    threshold=0.999,
    max_points=5000,
    device='cuda' if torch.cuda.is_available() else 'cpu'):
    """Adds embeddings for hidden space visualization."""

    FIXED_NOISE_GIANT = torch.randn(max_points, h_dim).to(device)
    
    with torch.no_grad():
        imgs = (generator(FIXED_NOISE_GIANT).clone().detach() + 1) / 2

        classifier = LeNet_BN().to(device)
        classifier_params = torch.load('LeNet_BN.pth', map_location=device)
        classifier.load_state_dict(classifier_params['model'])
        classifier.eval()
        classes = classifier(tv.transforms.Resize((28, 28))(imgs))

        mask = torch.max(torch.softmax(classes, dim=1), dim=1).values > threshold  # filter only good ones
        
        classes = classes[mask].argmax(dim=1).int().to('cpu').numpy()
        mat = FIXED_NOISE_GIANT.clone().detach()[mask]
        label_img = imgs[mask]
    print(mat.shape[0], 'samples is used.')

    summary_writer.add_embedding(
        tag=tag, 
        mat=mat, 
        label_img=label_img, 
        metadata=classes,
        )
